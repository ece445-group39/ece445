# ECE445 Lab Notebook for Zutai Chen
## 2022-2-10 proposal document group meeting
We have decided the structure of our project including MCU, IMU, Bluetooth and IR subparts on two pcb boards. The type of module still needs to be determined.See flowchart and visual aid.
![visualaid](visualaid.png)
![](flowchart.png)
## 2022-2-15 project design group meeting
We discussed about the components to be used including [MCU](https://www.digikey.com/en/products/detail/stmicroelectronics/STM32L152RET6/5051409?utm_adgroup=STMicroelectronics&utm_source=google&utm_medium=cpc&utm_campaign=Dynamic%20Search_EN_Focus%20Suppliers&utm_term=&utm_content=STMicroelectronics&gclid=CjwKCAjw682TBhATEiwA9crl34jAkOOD3DKC1sHgf4tcUCkvQpNtxleU3rOH8Bad8bDAC2x1pZ59whoCm08QAvD_BwE), [IMU](https://www.digikey.co.th/en/products/detail/ceva-technologies-inc/FSM300/7917164), [Bluetooth](https://www.digikey.com/en/products/detail/microchip-technology/RN42HID-I-RM/3597485) and [IR](https://www.digikey.com/en/products/detail/broadcom-limited/APDS-9130/4357748).
Starting making schmetics for these parts on pcb.
## 2022-2-23 soldering assignment group meeting
We first time soldered small pcb assignment.
## 2022-2-24 PCB schmetic design group meeting
Finished collar part pcb schmetic design.\
![MCU](2-27mcusch.png)
![regu](2-27regu.png)\
![MCU_C](2-27mcucollarsch.png)
![BT](2-27btsch.png)\
![imu](2-27imusch.png)\
## 2022-2-28 PCB design group meeting
Continuous working on collar pcb for first round pcb order. Basically working on arranging components.
## 2022-3-1 PCB module ordering group meeting
Ordering MCU, BT, IMU modules. Since imu data needs to be precise, we are preparing to change a new [IMU](https://learn.adafruit.com/adafruit-bno055-absolute-orientation-sensor) type if current type does not work as our expected.\
Ordering [voltage regulators](https://www.digikey.com/en/products/detail/diodes-incorporated/AP7365-33WG-7/4249847) \
Ordering one [3.7V battery](https://www.digikey.com/en/products/detail/sparkfun-electronics/PRT-13851/6605199?utm_adgroup=Battery%20Products&utm_source=google&utm_medium=cpc&utm_campaign=Dynamic%20Search_EN_Product&utm_term=&utm_content=Battery%20Products&gclid=CjwKCAjw682TBhATEiwA9crl3z7ELSleVuRfMiVjyjgM2dTmD6ffzRm9FEze9t9nGNuOTn5rj0bW5xoCyyYQAvD_BwE) and one [6V battery](https://www.digikey.com/en/products/detail/b-b-battery/HR9-6-T2/761707)\
Ordering a new [4.5V motors](https://datasheets.globalspec.com/ds/2353/DigiKey/BF7B6174-503F-4DC6-8B9D-221A0194C20B) instead of previous [6V motors](https://www.digikey.com/en/products/detail/pololu-corporation/999/10449927) and [lasers](https://www.digikey.com/en/products/detail/adafruit-industries-llc/1057/5638297)
## 2022-3-2 Online progress meeting
Discussing about mechenical part: a dispensor with laser diodes.\
Our plan is to reform [an M&M dispensor](https://www.mms.com/en-us/mms-fun-machine-dispenser/p/fun-machine-dispenser?gclid=CjwKCAiAyPyQBhB6EiwAFUuakquDJ-snw6NYC3L2aANTtrZp-JuCdPPlOJlHjEwIghLpOaRyOUn4txoCbdoQAvD_BwE&gclsrc=aw.ds)\
![oridisp](oridispenser.png)\
The original mechanism is pushing button to rotate the axis to allow the food drop from the platform. We plan to cut off the mechenical axis with our motors controlled by mcu to dispense food. Reference to [this Video](https://www.youtube.com/watch?v=XnjJZBxBtG0)\
For collar part we plan to use a [product collar](https://orbisify.com/product/pets-cat-shock-collar-automatic-meowing-preventer-trainer/?gclid=CjwKCAjw682TBhATEiwA9crl36wSP86w4P-YZoe1tlLQqLO0EImgX2TWSk3VPXH0wEoc5wPixKVVlRoC-EUQAvD_BwE) to contain our pcb.\
![catcollar](catcollar.jpg)\
## 2022-3-5 PCB group meeting
In progress of complishing PCB for collar part and revise base part schematics.
We changed our two voltage regulator design from 3.3V to [4.5V](https://www.mouser.com/ProductDetail/Microchip-Technology/MCP1727-5002E-SN?qs=AG1tZYOK7s5txqJbKF4ruw%3D%3D). Thus schematics have been revised below.\
![4.5vreg](4.5vreg.png)\
## 2022-3-7 Collar PCB order group meeting
Tommorow we will order our collar PCB first. Thus we meet to finish collar pcb design.
![cpcb](collarpcb.png)\
## 2022-3-9 pcb progress online meeting with TA
TA has suggest us to start on testing MCU. Our plan is to use [development board](https://www.mouser.com/ProductDetail/STMicroelectronics/NUCLEO-L452RE?qs=zwwtnr6GuIyXyX5dw%2FgKaw%3D%3D&mgh=1&gclid=CjwKCAjw682TBhATEiwA9crl39zcHOEDCng09PApvPcGgiJxD9xzi98b7S_ZgaeVAd8PivctiUbsThoChgMQAvD_BwE) for pretest.\
Also we will considering to communicate with machine shop for our mechanical part.\
## 2022-3-14 Base PCB order group meeting
We finished our Base part PCB and orderred by ourselves.\
![bpcb](basepcb.png)\
## 2022-3-19 Battery redesign group meeting
Our battery needs to be changed due to out of stock. Our [new 6V battery](https://www.digikey.com/en/products/detail/b-b-battery/BP7-6-T1/653324) has reduced in 1Ah(from 8Ah to 7Ah).
## 2022-3-24 to 2022-3-28 Solder Two PCBs 
I soldered the two PCBs for the collar and the base unit. \
![Collar_PCB](IMG_1334.JPG) \
![Base_PCB](IMG_1335.JPG) \
## 2022-3-29 Buy other hardware parts
I ordered the programming header, ST-LINK, and all other items necessary for our project. 
## 2022-3-30 Start Working on Initializing the IMU
Because Natalie kept getting frozen data from the BNO055, I decided to help her debugging first. I read the datasheet for the IMU and watched tutorials online. I found another project created by Kevin Townsend, which is to called Adafruit BNO055 Absolute Orientation Sensor, that can test out if our BNO055 can work functionally. It turned out to be working fine, so we knew that there is nothing wrong with our IMU. 
## 2022-3-31 Continue Working on Initializing the IMU
I used the built-in examples on Arduino to read the raw data of BNO055, and it seems to work fine as well. We again verified that the IMU should be able to work on STM32Cubeide as well. 
## 2022-4-1 Look for better IR Sensors
Our first-choice IR sensor was very hard to be soldered. It could break very easily, and it broke several times when Yuhan was testing it. We then decided to change our IR sensor from APDS9130 to APDS9960. I bought three APDS9960 at a time just in case if we accidentally break one of them. 
## 2022-4-2 Buy modules for all hardware parts
To ensure that each part can work as desired, I bought modules for all hardware parts. Because many parts we bought before cannot be used on a breadboard, I bought more for testing.  
## 2022-4-3 Find the dispenser to be used for our final product
Because we didn't talk to the machine shop, we had to solve for the mechanical part ourselves as well. I found a good dispenser that can be modified to get our final product. I have also designed the basic idea of how our mechanical part should look like. 
## 2022-4-4 Start Working on Pairing the Two Bluetooth Modules
I started to pair the two RN42 for the final project. 
## 2022-4-5 to 2022-4-8 Continue Working on Pairing the Two BT
I found that it is not possible to pair them together, because to do so, it requires to have its authenfication removed. However, since I cannot use jumper wires to connect the RN42 on a breadboard, there is no way we can do it. I decided to use another BT that I can remove its authenfication and have them paired together. 
## 2022-4-9 to 2022-4-10 Change BT from RN42 to HC-05
After I watched many tutorials and read the datasheet for many different options, I decided to use HC-05. It is a BT module that is easy to use, and other people have confirmed that two HC-05 can be paired together. I bought two HC-05. 
## 2022-4-11 to 2022-4-13 Watch more tutorials on HC-05
Before the new BT modules came, I watched more tutorial videos to know HC-05 better. I have downloaded some sample codes that can be used later on our HC-05s. 
## 2022-4-14 to 2022-4-16 Pair the Two BTs
I entered the command modes for the two BTs and had them paired successfully. 
## 2022-4-17 to 2022-4-21 Work on Data Transmission Between the Two BTs
Although the two BTs were paired, they could not successfully transmit any data. 
## 2022-4-22 Mock Demo 
Except the data transmission, all other parts of our project worked fine. 
## 2022-4-23 Test BTs on Arduino dev boards
After I searched up many websites, I found many users on ST Community had the same issues as we did. Some STM MCU just have this issue with the UART transmission, and I decided to test the two BT modules on Arduino instead. We bought Arduino dev boards, and we tested the BTs on the Arduino dev boards, they worked fine. We then knew that it was the MCU problem on STM MCUs. 
## 2022-4-24 Integrate Everything on Arduino
We finalized our project and combined everything together. 
## 2022-4-25 Buy things for our final demo
We went to the store and got things we needed. 
## 2022-4-26 Final Demo 
![Dispenser](IMG_1336.JPG) \
![Coat](IMG_1337.JPG) \



