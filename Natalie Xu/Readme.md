# ECE445 Lab Notebook for Natalie Xu
## 2022-2-10 proposal document group meeting
We have decided the structure of our project including MCU, IMU, Bluetooth and IR subparts on two pcb boards. The type of module still needs to be determined.See flowchart and visual aid.
![visualaid](visualaid.png)
![](flowchart.png)
## 2022-2-15 project design group meeting
We discussed about the components to be used including [MCU](https://www.digikey.com/en/products/detail/stmicroelectronics/STM32L152RET6/5051409?utm_adgroup=STMicroelectronics&utm_source=google&utm_medium=cpc&utm_campaign=Dynamic%20Search_EN_Focus%20Suppliers&utm_term=&utm_content=STMicroelectronics&gclid=CjwKCAjw682TBhATEiwA9crl34jAkOOD3DKC1sHgf4tcUCkvQpNtxleU3rOH8Bad8bDAC2x1pZ59whoCm08QAvD_BwE), [IMU](https://www.digikey.co.th/en/products/detail/ceva-technologies-inc/FSM300/7917164), [Bluetooth](https://www.digikey.com/en/products/detail/microchip-technology/RN42HID-I-RM/3597485) and [IR](https://www.digikey.com/en/products/detail/broadcom-limited/APDS-9130/4357748).
Starting making schmetics for these parts on pcb.
## 2022-2-23 soldering assignment group meeting
We first time soldered small pcb assignment.
## 2022-2-24 PCB schmetic design group meeting
Finished collar part pcb schmetic design.\
![MCU](2-27mcusch.png)
![regu](2-27regu.png)\
![MCU_C](2-27mcucollarsch.png)
![BT](2-27btsch.png)\
![imu](2-27imusch.png)\
## 2022-2-28 PCB design group meeting
Continuous working on collar pcb for first round pcb order. Basically working on arranging components.
## 2022-3-1 PCB module ordering group meeting
Ordering MCU, BT, IMU modules. Since imu data needs to be precise, we are preparing to change a new [IMU](https://learn.adafruit.com/adafruit-bno055-absolute-orientation-sensor) type if current type does not work as our expected.\
Ordering [voltage regulators](https://www.digikey.com/en/products/detail/diodes-incorporated/AP7365-33WG-7/4249847) \
Ordering one [3.7V battery](https://www.digikey.com/en/products/detail/sparkfun-electronics/PRT-13851/6605199?utm_adgroup=Battery%20Products&utm_source=google&utm_medium=cpc&utm_campaign=Dynamic%20Search_EN_Product&utm_term=&utm_content=Battery%20Products&gclid=CjwKCAjw682TBhATEiwA9crl3z7ELSleVuRfMiVjyjgM2dTmD6ffzRm9FEze9t9nGNuOTn5rj0bW5xoCyyYQAvD_BwE) and one [6V battery](https://www.digikey.com/en/products/detail/b-b-battery/HR9-6-T2/761707)\
Ordering a new [4.5V motors](https://datasheets.globalspec.com/ds/2353/DigiKey/BF7B6174-503F-4DC6-8B9D-221A0194C20B) instead of previous [6V motors](https://www.digikey.com/en/products/detail/pololu-corporation/999/10449927) and [lasers](https://www.digikey.com/en/products/detail/adafruit-industries-llc/1057/5638297)
## 2022-3-2 Online progress meeting
Discussing about mechenical part: a dispensor with laser diodes.\
Our plan is to reform [an M&M dispensor](https://www.mms.com/en-us/mms-fun-machine-dispenser/p/fun-machine-dispenser?gclid=CjwKCAiAyPyQBhB6EiwAFUuakquDJ-snw6NYC3L2aANTtrZp-JuCdPPlOJlHjEwIghLpOaRyOUn4txoCbdoQAvD_BwE&gclsrc=aw.ds)\
![oridisp](oridispenser.png)\
The original mechanism is pushing button to rotate the axis to allow the food drop from the platform. We plan to cut off the mechenical axis with our motors controlled by mcu to dispense food. Reference to [this Video](https://www.youtube.com/watch?v=XnjJZBxBtG0)\
For collar part we plan to use a [product collar](https://orbisify.com/product/pets-cat-shock-collar-automatic-meowing-preventer-trainer/?gclid=CjwKCAjw682TBhATEiwA9crl36wSP86w4P-YZoe1tlLQqLO0EImgX2TWSk3VPXH0wEoc5wPixKVVlRoC-EUQAvD_BwE) to contain our pcb.\
![catcollar](catcollar.jpg)\
## 2022-3-5 PCB group meeting
In progress of complishing PCB for collar part and revise base part schematics.
We changed our two voltage regulator design from 3.3V to [4.5V](https://www.mouser.com/ProductDetail/Microchip-Technology/MCP1727-5002E-SN?qs=AG1tZYOK7s5txqJbKF4ruw%3D%3D). Thus schematics have been revised below.\
![4.5vreg](4.5vreg.png)\
## 2022-3-7 Collar PCB order group meeting
Tommorow we will order our collar PCB first. Thus we meet to finish collar pcb design.
![cpcb](collarpcb.png)\
## 2022-3-9 pcb progress online meeting with TA
TA has suggest us to start on testing MCU. Our plan is to use [development board](https://www.mouser.com/ProductDetail/STMicroelectronics/NUCLEO-L452RE?qs=zwwtnr6GuIyXyX5dw%2FgKaw%3D%3D&mgh=1&gclid=CjwKCAjw682TBhATEiwA9crl39zcHOEDCng09PApvPcGgiJxD9xzi98b7S_ZgaeVAd8PivctiUbsThoChgMQAvD_BwE) for pretest.\
Also we will considering to communicate with machine shop for our mechanical part.\
## 2022-3-14 Base PCB order group meeting
We finished our Base part PCB and orderred by ourselves.\
![bpcb](basepcb.png)\
## 2022-3-19 Battery redesign group meeting
Our battery needs to be changed due to out of stock. Our [new 6V battery](https://www.digikey.com/en/products/detail/b-b-battery/BP7-6-T1/653324) has reduced in 1Ah(from 8Ah to 7Ah). \
## 2022-3-24 Start Coding on IMU
I started to work on the development board NUCLEO-L152RE, which has the same MCU chip with the one on our PCB design. I read the datasheet of BNO055 and NUCLEO board and started creating the library to initialize BNO055. I set up pins based on our circuit schematics using the datasheet. \
[MCU_layout_pins](https://gitlab.com/ece445-group39/ece445/-/blob/main/Natalie%20Xu/mcu_pins_layout.pdf) \
## 2022-3-25 to 2022-3-28 Finish Coding on IMU
I continued to work on coding on IMU and finished coding on IMU. There were five files modified: BNO055.h, accel.h, accel.c, main.c, and main.h. \
BNO055.h is the library I created for the IMU, and accel.c and accel.h are the two files initializing the IMU and retrieving data from the IMU. main.c includes all the logic required. \
![main.c](IMU_Code.png) \
![BNO055.h](BNO055_library.png) \ 
## 2022-3-29 to 2022-4-05 Debug the Code for the Collar PCB
Because the IMU kept giving me a frozen data, and the data did not seem to be right either. I spent about a week to debug the whole system. The reason turned out to be that I put the NUCLEO dev board on a bread board causing all the pins being messed up. There was nothing wrong with our code, but I connected the BNO055 and the dev board wrong.  
## 2022-4-06 to 2022-4-08 Convert the Code for the Dev Board to the Actual PCB
Our design has some difference compared to the NUCLEO dev board, and I modified the pin setup and added some other codes to have it work on the real PCB. 
## 2022-4-09 Finish Coding on the Collar PCB. 
I finished coding for the Collar PCB by April 11. 
## 2022-4-10 Start Coding on the IR
I started coding on the IR sensor on the NUCLEO dev board first as well. We used APDS9960 for our IR sensor, and again I created an entire library for it on STM32Cubeide.  
## 2022-4-11 to 2022-4-14 Continue Coding on the IR
I continued to work on coding on IR. There were five files modified: APDS9960.h, proximity.h, proximity.c, main.c, and main.h. \
APDS9960.h contains the library for the IR sensor. proximity.c and proximity.h are the two files initializing the IR and retrieving proximity data from the IR. main.c and main.h are the two files for the main logic of the base unit. \
![main.c](main_IR.png) \
![proximity.c](proximity.png)\ 
![APDS9960.h](APDS9960.png) \ 
## 2022-4-15 Debug and Finish Coding on the IR. 
The code did not seem to work at first, and very soon we found that the IR itself was broken. After we switched three IR sensors, it eventually worked. There was almost no debugging for the IR part, and the code worked perfectly fine the first time we ran it.  
## 2022-4-16 to 2022-4-18 Convert the Code for the Dev Board to the Actual PCB 
There were a few changed to made for the real PCB. I have modified some of our pins setup and had it work on base PCB.  
## 2022-4-19 Start Working on Pairing the Two Bluetooth Modules
After finished my software coding part, I started to help my teammates pairing the two Bluetooth Modules. 
## 2022-4-20 Change Bluetooth Modules
After reading for resources for RN42, we figured that we will need to remove the authentication in order to have the two paired successfully. However, because we cannot use jumper wires to connect the Bluetooth Modules, we couldn't do it. We have changed our Bluetooth Modules to HC-05, and we worked on how to transmit data between the two BTs. 
## 2022-4-21 Create a Bluetooth sample code
To ensure if our BT worked as expected, I created a new STM32 project that simply receives and sends UART signal.  
## 2022-4-22 Mock demo 
We had our mock demo on Friday. By the time, we had our Collar part and base unit work fine separately. We used a Bluetooth module simulator to test out our system by the time, and by sending a simulated BT signal mannually, our system met all high level requirements and almost all subsystem requirements. 
## 2022-4-23 Continue Working on Pairing the Two BT. 
We found that there is something wrong inside of our STM NUCLEO MCU chip, and we decided to buy Arduino boards to test. We bought the Arduino boards and found that the two BTs worked fine on their MCU chip. 
## 2022-4-24 Code everything on Arduino UNO 
To better integrate our project, we decided to show the functionalities on Arduino dev boards for our final demo. I started to code everything (IMU, IR, motors, etcs.) on Arduino. \
![Arduino_IMU](Arduino_IMU.png)\ 
![Arduino_IR](Arduino_IR.png)\
## 2022-4-25 Buy things for our final demo and sew the coat. 
Zutai and I went to the store and bought things we need for the final demo. I also helped to sew the cat coat (collar) to make sure that IMU will not move unexpectedly inside of the pocket. 
## 2022-4-26 Final demo
## 2022-4-27 Prepare for the mock presentation
We created the slides for the mock presentation. 
## 2022-4-29 Mock presentation
## 2022-4-30 to 2022-5-1 Revise our slides
We revised and better prepared for our slides for the final presentation. 
## 2022-5-2 to 2022-5-4 Write the Final Paper
## 2022-5-4 Final Paper due
## 2022-5-5 Finish Git Notebook. 
