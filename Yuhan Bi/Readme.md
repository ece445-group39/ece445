# ECE445 Lab Notebook for Yuhan Bi
## 2022-2-10 proposal document group meeting
We have decided the structure of our project including MCU, IMU, Bluetooth and IR subparts on two pcb boards. The type of module still needs to be determined.See flowchart and visual aid.
![visualaid](visualaid.png)
![](flowchart.png)
## 2022-2-15 project design group meeting
We discussed about the components to be used including [MCU](https://www.digikey.com/en/products/detail/stmicroelectronics/STM32L152RET6/5051409?utm_adgroup=STMicroelectronics&utm_source=google&utm_medium=cpc&utm_campaign=Dynamic%20Search_EN_Focus%20Suppliers&utm_term=&utm_content=STMicroelectronics&gclid=CjwKCAjw682TBhATEiwA9crl34jAkOOD3DKC1sHgf4tcUCkvQpNtxleU3rOH8Bad8bDAC2x1pZ59whoCm08QAvD_BwE), [IMU](https://www.digikey.co.th/en/products/detail/ceva-technologies-inc/FSM300/7917164), [Bluetooth](https://www.digikey.com/en/products/detail/microchip-technology/RN42HID-I-RM/3597485) and [IR](https://www.digikey.com/en/products/detail/broadcom-limited/APDS-9130/4357748).
Starting making schmetics for these parts on pcb.
## 2022-2-23 soldering assignment group meeting
We first time soldered small pcb assignment.
## 2022-2-24 PCB schmetic design group meeting
Finished collar part pcb schmetic design.\
![MCU](2-27mcusch.png)
![regu](2-27regu.png)\
![MCU_C](2-27mcucollarsch.png)
![BT](2-27btsch.png)\
![imu](2-27imusch.png)
## 2022-2-28 PCB design group meeting
Continuous working on collar pcb for first round pcb order. Basically working on arranging components.
## 2022-3-1 PCB module ordering group meeting
Ordering MCU, BT, IMU modules. Since imu data needs to be precise, we are preparing to change a new [IMU](https://learn.adafruit.com/adafruit-bno055-absolute-orientation-sensor) type if current type does not work as our expected.\
Ordering [voltage regulators](https://www.digikey.com/en/products/detail/diodes-incorporated/AP7365-33WG-7/4249847) \
Ordering one [3.7V battery](https://www.digikey.com/en/products/detail/sparkfun-electronics/PRT-13851/6605199?utm_adgroup=Battery%20Products&utm_source=google&utm_medium=cpc&utm_campaign=Dynamic%20Search_EN_Product&utm_term=&utm_content=Battery%20Products&gclid=CjwKCAjw682TBhATEiwA9crl3z7ELSleVuRfMiVjyjgM2dTmD6ffzRm9FEze9t9nGNuOTn5rj0bW5xoCyyYQAvD_BwE) and one [6V battery](https://www.digikey.com/en/products/detail/b-b-battery/HR9-6-T2/761707)\
Ordering a new [4.5V motors](https://datasheets.globalspec.com/ds/2353/DigiKey/BF7B6174-503F-4DC6-8B9D-221A0194C20B) instead of previous [6V motors](https://www.digikey.com/en/products/detail/pololu-corporation/999/10449927) and [lasers](https://www.digikey.com/en/products/detail/adafruit-industries-llc/1057/5638297)
## 2022-3-2 Online progress meeting
Discussing about mechenical part: a dispensor with laser diodes.\
Our plan is to reform [an M&M dispensor](https://www.mms.com/en-us/mms-fun-machine-dispenser/p/fun-machine-dispenser?gclid=CjwKCAiAyPyQBhB6EiwAFUuakquDJ-snw6NYC3L2aANTtrZp-JuCdPPlOJlHjEwIghLpOaRyOUn4txoCbdoQAvD_BwE&gclsrc=aw.ds)\
![oridisp](oridispenser.png)\
The original mechanism is pushing button to rotate the axis to allow the food drop from the platform. We plan to cut off the mechenical axis with our motors controlled by mcu to dispense food. Reference to [this Video](https://www.youtube.com/watch?v=XnjJZBxBtG0)\
For collar part we plan to use a [product collar](https://orbisify.com/product/pets-cat-shock-collar-automatic-meowing-preventer-trainer/?gclid=CjwKCAjw682TBhATEiwA9crl36wSP86w4P-YZoe1tlLQqLO0EImgX2TWSk3VPXH0wEoc5wPixKVVlRoC-EUQAvD_BwE) to contain our pcb.\
![catcollar](catcollar.jpg)
## 2022-3-5 PCB group meeting
In progress of complishing PCB for collar part and revise base part schematics.
We changed our two voltage regulator design from 3.3V to [4.5V](https://www.mouser.com/ProductDetail/Microchip-Technology/MCP1727-5002E-SN?qs=AG1tZYOK7s5txqJbKF4ruw%3D%3D). Thus schematics have been revised below.\
![4.5vreg](4.5vreg.png)
## 2022-3-7 Collar PCB order group meeting
Tommorow we will order our collar PCB first. Thus we meet to finish collar pcb design.
![cpcb](collarpcb.png)
## 2022-3-9 pcb progress online meeting with TA
TA has suggest us to start on testing MCU. Our plan is to use [development board](https://www.mouser.com/ProductDetail/STMicroelectronics/NUCLEO-L452RE?qs=zwwtnr6GuIyXyX5dw%2FgKaw%3D%3D&mgh=1&gclid=CjwKCAjw682TBhATEiwA9crl39zcHOEDCng09PApvPcGgiJxD9xzi98b7S_ZgaeVAd8PivctiUbsThoChgMQAvD_BwE) for pretest.\
Also we will considering to communicate with machine shop for our mechanical part.
## 2022-3-14 Base PCB order group meeting
We finished our Base part PCB and orderred by ourselves.\
![bpcb](basepcb.png)
## 2022-3-19 Battery redesign group meeting
Our battery needs to be changed due to out of stock. Our [new 6V battery](https://www.digikey.com/en/products/detail/b-b-battery/BP7-6-T1/653324) has reduced in 1Ah(from 8Ah to 7Ah).
## 2022-3-27 
Solder and test voltage regulator on collar pcb.
![vrpcb](vrcollar.png)
## 2022-3-29
Trying to pair two bluetooth modules on PCB and failed.\
I flyed two wires from GPIO3 and GPIO6 in order to start autopairing program.But the BT module shows no work.
![GPIO3](gpio3.png)
## 2022-4-4
Testing IR working well. As I put my hand infront of it, the hot resource forced the current to increase to over 1.44V from standby 0.8V.\
See ![](irtest.mp4)
## 2022-4-10
Since our on board bluetooth not working, my job changed to process mechanical part. I have started from cutting the top button to figure out the axis structure.
## 2022-4-13
The motor successfully attached to the dispensor axis.
![disp1](disp1.png)
## 2022-4-17
Since the motor could not active inversely, I plan to use a rubber string to strech the axis back when motor is not working. Also, the input and output wires requires to be firmly attached. An iron sheet is used to fix position.\
![disp2](disp2.png)
## 2022-4-18
Trying to attach the laser with motors on dispensor. 
In order to let the laser ossilate in line rather than rotating, I left the motor gear for only 1/4 teeth. When teeth bite each other, the laser will be lifted up. Otherwise the laser will drop.\
![disp3](disp3.png)
## 2022-4-19
Use breadboard to test and adjust dispensor working. Using DC 6V given by multimeter connect to pcb and output voltage from voltage regulator VDD pin. A fatal error occurs that the output regulated voltage could not provide enough power to rotate the motor. The current drop under 0.2A.\
![](disptest1.mp4)
## 2022-4-20
I decide to use MOSFET as switch instead of voltage regulator. Connecting the Drain to 6V DC and Source to motor VDD, by given a 3.3V signal to Gate, the gs voltage would reach threshold and enable the motor.
![](disptest2.mp4)
## 2022-4-22
Demoed working of laser and dispensor seperately(not controlled by mcu)
![](lasertest.mp4)
## 2022-4-25
Composed all parts together for demo.
![](compose.mp4)
## 2022-4-28
Obtain datas for mock presentation
motor resistance![](d1.png)\
laser motor voltage![](d2.png)\
laser and motor resistance![](d3.png)\
3.7V voltage output![](d6.png)
